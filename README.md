# LevelDB for .NET (discontinued) #

This used to be my .NET wrapper for LevelDB.
It is no longer maintained.
Please use one of the projects below:

* [leveldb-sharp - .NET/C# wrapper](https://www.meebey.net/projects/leveldb-sharp/)
* [LevelDB.Net - C++/CLI port](https://github.com/neo-project/leveldb/tree/cli)
* [LevelDB for Windows and .NET](https://github.com/Reactive-Extensions/LevelDB)
* [Official Windows branch from Google (C/C++)](https://github.com/google/leveldb/tree/windows)
* [Updated fork of my LevelDB port for Visual Studio (C++)](https://github.com/ren85/leveldb-windows)

